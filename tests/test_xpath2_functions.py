__author__ = 'kamil'
from unittest2 import TestCase

from lxml import etree
from lxml.etree import fromstring as str2xml, XPathEvalError

import xpath2_functions


class Xpath2Functions(TestCase):

    def test_functions_argument(self):
        xpath2_functions.register_functions(etree, functions=[])
        msg = str2xml('''<msg>
                            <out><status>X</status></out>
                            <out><status>Y</status></out>
                            <out><status>Z</status></out>
                         </msg>''')
        self.assertRaisesRegexp(
            XPathEvalError, 'Unregistered function',
            msg.xpath, 'xp2f:string-join(//status/text(), ",")'

        )

    def test_functions_empty_ns(self):
        xpath2_functions.register_functions(etree, ns=None)
        msg = str2xml('''<msg>
                            <out><status>X</status></out>
                            <out><status>Y</status></out>
                            <out><status>Z</status></out>
                         </msg>''')
        self.assertEqual(msg.xpath('string-join(//status/text(), ",")'), 'X,Y,Z')

    def test_join_string(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('''<msg>
                            <out><status>X</status></out>
                            <out><status>Y</status></out>
                            <out><status>Z</status></out>
                         </msg>''')
        self.assertEqual(msg.xpath('xp2f:string-join(//status/text(), ",")'),
                         'X,Y,Z')
        self.assertEqual(msg.xpath('xp2f:string-join(//status, ",")'),
                         'X,Y,Z')
        self.assertEqual(msg.xpath('xp2f:string-join(//out, ",")'),
                         'X,Y,Z')
        self.assertEqual(msg.xpath('xp2f:string-join(//out2, ",")'),
                         '')

        self.assertRaisesRegexp(
            TypeError, 'string_join\(\) takes exactly 3 arguments \(2 given\)',
            msg.xpath, 'xp2f:string-join(//out2)')

    def test_lowercase_string(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('''<msg>
                            <out><status>X</status></out>
                         </msg>''')
        self.assertEqual(msg.xpath('xp2f:lower-case(//status/text())'),
                         ['x'])
